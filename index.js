const express = require('express');
const { User, Item, Order, OrderItem } = require('./models');
const bcrypt = require('bcrypt');
const app = express();
const PORT = process.env.PORT || 3000;

app.use(express.json());

// API untuk login pengguna
app.post('/login', async (req, res) => {
  try {
    const { email, password } = req.body;

    // Cek apakah user ada di database
    const user = await User.findOne({ where: { email } });
    if (!user) {
        return res.status(401).json({ message: 'Email dan Password Salah' });
    }

    // Cek jika password benar atau salah
    const isValidPassword = await bcrypt.compareSync(password, user.password);
    if (!isValidPassword) {
        return res.status(401).json({ message: 'Email dan Password Salah' });
    }

    res.status(200).json({ message: 'Login successful', user });
  } catch (error) {
    console.error('Error in login:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
  });


// API untuk membaca semua pengguna
app.get('/users', async (req, res) => {
  try {
    const users = await User.findAll();
    res.json(users);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

// API untuk menyimpan pengguna baru
app.post('/register', async (req, res) => {
  const { name, email, password, address, no_hp } = req.body;

  try {
    // Check apakah email sudah terdaftar
    const existingUser = await User.findOne({ 
        where: { email } 
    });
    if (existingUser) {
      return res.status(400).json({ 
        error: 'Email already exists'
    });
    }

    // Buat pengguna baru
    const newUser = await User.create({
      name,
      email,
      password: bcrypt.hashSync(password, 10),
      address,
      no_hp,
    });

    res.status(201).json(newUser);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
});

// API untuk memperbarui pengguna
app.put('/users/:userId', async (req, res) => {
  const { userId } = req.params;
  const { name, email, password, address, no_hp } = req.body;

  try {
    const user = await User.findByPk(userId);
    if (!user) {
      return res.status(404).json({ error: 'User not found' });
    }

    if (password) {
      req.body.password = await password;
    }

    await user.update(req.body);

    res.json(user);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
});

// API untuk menghapus pengguna
app.delete('/users/:userId', async (req, res) => {
  const { userId } = req.params;
  const { password } = req.body;
  try {
    const user = await User.findByPk(userId);
    if (!user) {
      return res.status(404).json({ error: 'User not found' });
    }

    if (password) {
        req.body.password = await password;
    }

    await user.destroy();

    res.json({ message: 'User deleted successfully' });
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
});

// API untuk membaca semua barang
app.get('/items', async (req, res) => {
  try {
    const items = await Item.findAll();
    res.json(items);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

// API untuk menyimpan barang baru
app.post('/items', async (req, res) => {
  const newItem = req.body;

  try {
    const item = await Item.create(newItem);
    res.status(201).json(item);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
});

// API untuk memperbarui barang
app.put('/items/:itemId', async (req, res) => {
  const { itemId } = req.params;
  const updatedItem = req.body;

  try {
    const item = await Item.findByPk(itemId);
    if (!item) {
      return res.status(404).json({ error: 'Item not found' });
    }

    await item.update(updatedItem);

    res.json(item);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
});

// API untuk menghapus barang
app.delete('/items/:itemId', async (req, res) => {
  const { itemId } = req.params;

  try {
    const item = await Item.findByPk(itemId);
    if (!item) {
      return res.status(404).json({ error: 'Item not found' });
    }

    await item.destroy();

    res.json({ message: 'Item deleted successfully' });
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
});

// API untuk membaca semua pesanan
app.get('/orders', async (req, res) => {
  try {
    const orders = await Order.findAll();
    res.json(orders);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

// API untuk menyimpan pesanan baru
app.post('/orders', async (req, res) => {
  const newOrder = req.body;

  try {
    const order = await Order.create(newOrder);
    res.status(201).json(order);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
});

// API untuk memperbarui pesanan
app.put('/orders/:orderId', async (req, res) => {
  const { orderId } = req.params;
  const updatedOrder = req.body;

  try {
    const order = await Order.findByPk(orderId);
    if (!order) {
      return res.status(404).json({ error: 'Order not found' });
    }

    await order.update(updatedOrder);

    res.json(order);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
});

// API untuk menghapus pesanan
app.delete('/orders/:orderId', async (req, res) => {
  const { orderId } = req.params;

  try {
    const order = await Order.findByPk(orderId);
    if (!order) {
      return res.status(404).json({ error: 'Order not found' });
    }

    await order.destroy();

    res.json({ message: 'Order deleted successfully' });
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
});

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
